package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    @Override
    Task findOneById(final @NotNull String ownerId, final @NotNull String id);

    @Override
    Task findOneByName(final @NotNull String name);

    @Override
    @Nullable List<Task> findAll(final @NotNull String ownerId);

    @Override
    @Nullable List<Task> findAllByName(@NotNull String ownerId, @NotNull String text);

    @Override
    @Nullable List<Task> findAllByDescription(@NotNull String ownerId, @NotNull String text);

    @Override
    void persist(final @NotNull String ownerId, final @NotNull Task entity);

    @Override
    void merge(final @NotNull String ownerId, final @NotNull String newData, final @NotNull Task entityToMerge, final @NotNull DataType dataType);

    @Override
    void remove(final @NotNull String ownerId, final @NotNull Task entity);

    @Override
    void removeAll(final @NotNull String ownerId);
}
