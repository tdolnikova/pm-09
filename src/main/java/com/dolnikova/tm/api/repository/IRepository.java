package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @Nullable
    E findOneById(@NotNull final String ownerId, @NotNull final String id);

    @Nullable
    E findOneByName(@NotNull final String name);

    @Nullable
    List<E> findAll(@NotNull final String ownerId);

    @Nullable
    List<E> findAllByName(@NotNull String ownerId, @NotNull String text);

    @Nullable
    List<E> findAllByDescription(@NotNull String ownerId, @NotNull String text);

    void persist(@NotNull final String ownerId, @NotNull final E entity);

    void merge(@NotNull final String ownerId, @NotNull final String newData, @NotNull final E entityToMerge, @NotNull final DataType dataType);

    void remove(@NotNull final String ownerId, @NotNull final E entity);

    void removeAll(@NotNull final String ownerId);

}
