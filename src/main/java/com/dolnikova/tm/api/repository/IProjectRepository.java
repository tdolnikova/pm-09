package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    @Override
    Project findOneById(final @NotNull String ownerId, final @NotNull String id);

    @Override
    Project findOneByName(final @NotNull String name);

    @Override
    @Nullable List<Project> findAll(final @NotNull String ownerId);

    @Override
    @Nullable List<Project> findAllByName(@NotNull String ownerId, @NotNull String text);

    @Override
    @Nullable List<Project> findAllByDescription(@NotNull String ownerId, @NotNull String text);

    @Override
    void persist(final @NotNull String ownerId, final @NotNull Project entity);

    @Override
    void merge(final @NotNull String ownerId, final @NotNull String newData, final @NotNull Project entityToMerge, final @NotNull DataType dataType);

    @Override
    void remove(final @NotNull String ownerId, final @NotNull Project entity);

    @Override
    void removeAll(final @NotNull String ownerId);
}
