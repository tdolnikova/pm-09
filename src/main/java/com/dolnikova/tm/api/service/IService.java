package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @Nullable
    E findOneById(@Nullable final String ownerId, @Nullable final String id);

    @Nullable
    E findOneByName(@Nullable final String name);

    @Nullable
    List<E> findAll(@Nullable final String ownerId);

    @Nullable
    List<E> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Nullable
    List<E> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    void persist(@Nullable final String ownerId, @Nullable final E entity);

    void merge(@Nullable final String ownerId, @Nullable final String newData, @Nullable final E entityToMerge, @Nullable final DataType dataType);

    void remove(@Nullable final String ownerId, @Nullable final E entity);

    void removeAll(@Nullable final String ownerId);

}
