package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    @Override
    User findOneById(final @Nullable String ownerId, final @Nullable String id);

    @Override
    User findOneByName(final @Nullable String name);

    @Override
    @Nullable List<User> findAll(final @Nullable String ownerId);

    @Override
    @Nullable List<User> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Override
    @Nullable List<User> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(final @Nullable String ownerId, final @Nullable User entity);

    @Override
    void merge(final @Nullable String ownerId, final @Nullable String newData, final @Nullable User entityToMerge, final @Nullable DataType dataType);

    @Override
    void remove(final @Nullable String ownerId, final @Nullable User entity);

    @Override
    void removeAll(final @Nullable String ownerId);

    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable final User currentUser);

}
