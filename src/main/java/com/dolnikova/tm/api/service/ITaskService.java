package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task>, Comparable<Task> {

    @Nullable
    @Override
    Task findOneById(final @Nullable String ownerId, final @Nullable String id);

    @Override
    Task findOneByName(final @Nullable String name);

    @Override
    @Nullable List<Task> findAll(final @Nullable String ownerId);

    @Override
    @Nullable List<Task> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Override
    @Nullable List<Task> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(final @Nullable String ownerId, final @Nullable Task entity);

    @Override
    void merge(final @Nullable String ownerId, final @Nullable String newData, final @Nullable Task entityToMerge, final @Nullable DataType dataType);

    @Override
    void remove(final @Nullable String ownerId, final @Nullable Task entity);

    @Override
    void removeAll(final @Nullable String ownerId);

    @Override
    Comparator<Task> getStatusComparator(final boolean direction);

    @Override
    Comparator<Task> getCreationDateComparator(final boolean direction);

    @Override
    Comparator<Task> getStartDateComparator(final boolean direction);

    @Override
    Comparator<Task> getEndDateComparator(final boolean direction);
}
