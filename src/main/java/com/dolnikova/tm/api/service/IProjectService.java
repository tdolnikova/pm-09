package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project>, Comparable<Project> {

    @Nullable
    @Override
    Project findOneById(@Nullable final String userId, @Nullable final String id);

    @Override
    Project findOneByName(final @Nullable String name);

    @Nullable
    @Override
    List<Project> findAll(@Nullable final String userId);

    @Override
    @Nullable List<Project> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Override
    @Nullable List<Project> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(@Nullable final String userId, @Nullable final Project entity);

    @Override
    void merge(@Nullable final String userId, @Nullable final String newData, @Nullable final Project entityToMerge, @Nullable final DataType dataType);

    @Override
    void remove(@Nullable final String userId, @Nullable final Project entity);

    @Override
    void removeAll(@Nullable final String userId);

    @Override
    Comparator<Project> getStatusComparator(final boolean direction);

    @Override
    Comparator<Project> getCreationDateComparator(final boolean direction);

    @Override
    Comparator<Project> getStartDateComparator(final boolean direction);

    @Override
    Comparator<Project> getEndDateComparator(final boolean direction);

}
