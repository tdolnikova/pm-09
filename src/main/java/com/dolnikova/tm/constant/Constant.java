package com.dolnikova.tm.constant;

public final class Constant {

    /*
     * General messages
     * */

    public final static String ID = "ID";
    public final static String COLON = ": ";
    public final static String INCORRECT_NUMBER = "Некорректный ввод.";
    public final static String TRY_AGAIN = "Попробуйте еще раз.";
    public final static String CHOOSE_SORTING_TYPE = "Выберите вид сортировки";
    public final static String INSERT_TEXT = "Введите текст.";

    /*
     * Help messages
     * */

    public final static String HELP = "help";
    public final static String HELP_DESCRIPTION = "show all commands";

    public final static String ABOUT = "about";
    public final static String ABOUT_DESCRIPTION = "show info about the app";

    public final static String FIND_ALL_PROJECTS = "find all projects";
    public final static String FIND_ALL_PROJECTS_DESCRIPTION = "find all projects";
    public final static String FIND_PROJECT = "find project";
    public final static String FIND_PROJECT_DESCRIPTION = "find a project";
    public final static String FIND_PROJECT_BY_NAME = "find project by name";
    public final static String FIND_PROJECT_BY_NAME_DESCRIPTION = "find a project by name";
    public final static String FIND_PROJECT_BY_DESCRIPTION = "find project by desc";
    public final static String FIND_PROJECT_BY_DESCRIPTION_DESCRIPTION = "find a project by description";
    public final static String PERSIST_PROJECT = "create project";
    public final static String PERSIST_PROJECT_DESCRIPTION = "create a project";
    public final static String MERGE_PROJECT = "update project";
    public final static String MERGE_PROJECT_DESCRIPTION = "update a project";
    public final static String REMOVE_PROJECT = "delete project";
    public final static String REMOVE_PROJECT_DESCRIPTION = "delete a project";
    public final static String REMOVE_ALL_PROJECTS = "delete all projects";
    public final static String REMOVE_ALL_PROJECTS_DESCRIPTION = "delete all projects";
    public final static String SORT_PROJECTS = "sort projects";
    public final static String SORT_PROJECTS_DESCRIPTION = "sort projects";

    public final static String FIND_ALL_TASKS = "find all tasks";
    public final static String FIND_ALL_TASKS_DESCRIPTION = "find all tasks";
    public final static String FIND_TASK = "find task";
    public final static String FIND_TASK_DESCRIPTION = "find a task";
    public final static String FIND_TASK_BY_NAME = "find task by name";
    public final static String FIND_TASK_BY_NAME_DESCRIPTION = "find a task by name";
    public final static String FIND_TASK_BY_DESCRIPTION = "find task by desc";
    public final static String FIND_TASK_BY_DESCRIPTION_DESCRIPTION = "find a task by description";
    public final static String PERSIST_TASK = "create task";
    public final static String PERSIST_TASK_DESCRIPTION = "create a task";
    public final static String MERGE_TASK = "update task";
    public final static String MERGE_TASK_DESCRIPTION = "update a task";
    public final static String REMOVE_TASK = "delete task";
    public final static String REMOVE_TASK_DESCRIPTION = "delete a task";
    public final static String REMOVE_ALL_TASKS = "delete all tasks";
    public final static String REMOVE_ALL_TASKS_DESCRIPTION = "delete all tasks";
    public final static String SORT_TASKS = "sort tasks";
    public final static String SORT_TASKS_DESCRIPTION = "sort tasks";

    public final static String EXIT = "exit";
    public final static String EXIT_DESCRIPTION = "exit";

    /*
    * Project messages
    * */

    public final static String CHOOSE_PROJECT = "Выберите проект.";
    public final static String INSERT_START_DATE = "Введите дату начала.";
    public final static String INSERT_END_DATE = "Введите дату окончания.";
    public final static String PROJECT_NAME = "Найден проект: ";
    public final static String IN_PROJECT = "В проекте ";
    public final static String PROJECT = "Проект";
    public final static String CREATED_M = "создан.";
    public final static String NO_PROJECTS = "Проектов нет.";
    public final static String PROJECT_UPDATED = "Проект обновлен.";
    public final static String PROJECT_DELETED = "Проект удален.";
    public final static String INSERT_PROJECT_NAME = "Введите название проекта.";
    public final static String PROJECT_NAME_DOESNT_EXIST = "Проекта с таким названием не существует.";
    public final static String PROJECT_NAME_ALREADY_EXIST = "Проект с таким названием уже существует.";
    public final static String WHICH_PROJECT_DELETE = "Какой проект хотите удалить?";
    public final static String NO_TASKS_IN_PROJECT = "В выбранном проекте нет задач.";

    /*
     * Task messages
     * */

    public final static String TASK = "Задача";
    public final static String NO_TASKS = "Нет задач.";
    public final static String TASK_ADDITION_COMPLETED = "Создание задач завершено.";
    public final static String CREATED_F = "создана.";
    public final static String INSERT_TASK = "Введите задачу: ";
    public final static String INSERT_NEW_TASK = "Введите новую задачу.";
    public final static String INSERT_TASK_ID = "Введите id задачи.";
    public final static String INSERT_NEW_PROJECT_NAME = "Введите название нового проекта.";
    public final static String TASK_UPDATED = "Задача изменена.";
    public final static String TASK_DELETED = "Задача удалена.";
    public final static String OF_TASKS_WITH_POINT = "задач(и)";

    /*
     * User messages
     * */

    public final static String USER_AUTH = "auth";
    public final static String USER_AUTH_DESCRIPTION = "authorization";
    public final static String USER_REG = "reg";
    public final static String USER_REG_DESCRIPTION = "registration";

    public final static String USER_CHANGE_PASSWORD = "change password";
    public final static String USER_CHANGE_PASSWORD_DESCRIPTION = "change password";
    public final static String USER_EDIT_PROFILE = "edit profile";
    public final static String USER_EDIT_PROFILE_DESCRIPTION = "edit profile";
    public final static String USER_FIND_PROFILE = "show profile";
    public final static String USER_FIND_PROFILE_DESCRIPTION = "show profile";
    public final static String USER_SIGN_OUT = "sign out";
    public final static String USER_SIGN_OUT_DESCRIPTION = "sign out";

    /*
     * Registration and authorization
     * */

    public final static String REG_ENTER_LOGIN = "Введите логин.";
    public final static String REG_ENTER_PASSWORD = "Введите пароль.";
    public final static String REG_USER_EXISTS = "Пользователь с таким именем существует.";
    public final static String REG_USER_NOT_FOUND = "Пользователь не найден.";
    public final static String REG_USER_REGISTERED = "Пользователь зарегистрирован. Войдите в систему.";
    public final static String REG_USER_SUCCESSFUL_LOGIN = "Вход выполнен.";


    public final static String ENTER_OLD_PASSWORD = "Введите старый пароль.";
    public final static String ENTER_NEW_PASSWORD = "Введить новый пароль.";
    public final static String PASSWORD_CHANGED_SUCCESSFULLY = "Пароль изменен.";
    public final static String WRONG_PASSWORD = "Неверный пароль.";
    public final static String LOGIN_CHANGED = "Логин изменен.";

    public final static String ENTER_FIELD_NAME = "Выберите поле для редактирования:";

    public final static String ENTER = "Введите ";
    public final static String ROLE_CHANGED = "Роль изменена.";


}
