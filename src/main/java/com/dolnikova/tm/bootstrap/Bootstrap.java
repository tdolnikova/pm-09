package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.api.bootstrap.ServiceLocator;
import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.exception.CommandCorruptException;
import com.dolnikova.tm.exception.NoAuthorizationException;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.TaskRepository;
import com.dolnikova.tm.repository.UserRepository;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.TaskService;
import com.dolnikova.tm.service.UserService;
import com.dolnikova.tm.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.util.*;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    public final static Scanner scanner = new Scanner(System.in);
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("com.dolnikova.tm").getSubTypesOf(AbstractCommand.class);

    public void init(@NotNull Class[] CLASSES) {
        @NotNull final User user = new User("user", "user", Role.USER);
        registryUser(user);
        userService.setCurrentUser(user);
        registryUser(new User("admin", "admin", Role.ADMIN));

        insertDullData(user);

        try {
            for (@NotNull final Class clazz : CLASSES) {
                if (!AbstractCommand.class.isAssignableFrom(clazz)) continue;
                registryCommand((AbstractCommand) clazz.newInstance());
            }
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registryUser(@NotNull final User user) {
        if (user.getName() == null || user.getName().isEmpty()) return;
        if (user.getDescription() == null || user.getDescription().isEmpty()) return;
        if (user.getRole() == null) return;
        userService.persist(user.getId(), user);
    }

    private void registryCommand(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @Nullable final String cliCommand = command.command();
        @Nullable final String cliDescription = command.description();
        if (cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            try {
                command = scanner.nextLine();
                if (command.isEmpty()) continue;
                if (commands.get(command) == null)
                    throw new CommandCorruptException();
                if (userService.getCurrentUser() == null && !commands.get(command).isSecure())
                    throw new NoAuthorizationException();
                execute(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    private void insertDullData(User user) {
        Project project = new Project(user.getId(), "qwerty");
        project.setStartDate(DateUtil.stringToDate("01.01.1998"));
        project.setEndDate(DateUtil.stringToDate("12.05.1999"));
        projectService.persist(user.getId(), project);
        Task task1 = new Task(user.getId(), "task1");
        task1.setProjectId(project.getId());
        task1.setStartDate(DateUtil.stringToDate("03.11.2019"));
        task1.setEndDate(DateUtil.stringToDate("03.07.2020"));
        Task task2 = new Task(user.getId(), "task2");
        task2.setProjectId(project.getId());
        task2.setStartDate(DateUtil.stringToDate("10.06.1999"));
        task2.setEndDate(DateUtil.stringToDate("12.08.2000"));
        taskService.persist(user.getId(), task1);
        taskService.persist(user.getId(), task2);
    }

}
