package com.dolnikova.tm;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.about.AboutCommand;
import com.dolnikova.tm.command.exit.ExitCommand;
import com.dolnikova.tm.command.help.HelpCommand;
import com.dolnikova.tm.command.project.*;
import com.dolnikova.tm.command.task.*;
import com.dolnikova.tm.command.user.*;
import org.jetbrains.annotations.NotNull;

public final class Application {

    @NotNull
    private static final Class[] CLASSES = {
            HelpCommand.class,
            ExitCommand.class,
            AboutCommand.class,

            UserAuthCommand.class, UserChangePasswordCommand.class,
            UserEditProfileCommand.class, UserFindProfileCommand.class,
            UserRegisterCommand.class, UserSignOutCommand.class,

            ProjectFindAllCommand.class, ProjectFindOneCommand.class,
            ProjectMergeCommand.class, ProjectPersistCommand.class,
            ProjectRemoveAllCommand.class, ProjectRemoveCommand.class,
            ProjectSortListCommand.class, ProjectFindByNameCommand.class,
            ProjectFindByDescriptionCommand.class,

            TaskFindAllCommand.class, TaskFindOneCommand.class,
            TaskMergeCommand.class, TaskPersistCommand.class,
            TaskRemoveAllCommand.class, TaskRemoveCommand.class,
            TaskSortListCommand.class, TaskFindByNameCommand.class,
            TaskFindByDescriptionCommand.class};

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }

}
