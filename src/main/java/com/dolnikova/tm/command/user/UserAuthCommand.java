package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserAuthCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.USER_AUTH;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.USER_AUTH_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[AUTHORIZATION]");
        System.out.println(Constant.REG_ENTER_LOGIN);
        @NotNull String name = "";
        while (name.isEmpty()) {
            name = Bootstrap.scanner.nextLine();
        }
        System.out.println(Constant.REG_ENTER_PASSWORD);
        @NotNull String description = "";
        while (description.isEmpty()) {
            description = Bootstrap.scanner.nextLine();
        }
        @Nullable final User user = serviceLocator.getUserService().findOneByName(name);
        if (user == null) {
            System.out.println(Constant.REG_USER_NOT_FOUND);
            return;
        }
        if (!user.getDescription().equals(HashUtil.stringToHashString(description))) {
            System.out.println(Constant.WRONG_PASSWORD);
            return;
        }
        serviceLocator.getUserService().setCurrentUser(user);
        System.out.println(Constant.REG_USER_SUCCESSFUL_LOGIN);
    }

    @Override
    public boolean isSecure() {
        return serviceLocator.getUserService().getCurrentUser() == null;
    }
}
