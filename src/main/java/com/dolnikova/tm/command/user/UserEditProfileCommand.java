package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserEditProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.USER_EDIT_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.USER_EDIT_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.ENTER_FIELD_NAME + "\n" + DataType.NAME);
        @NotNull String fieldToEdit = "";
        while (fieldToEdit.isEmpty()) {
            fieldToEdit = Bootstrap.scanner.nextLine();
        }
        if (fieldToEdit.equals(DataType.NAME.toString())) {
            changeData(DataType.NAME);
            System.out.println(Constant.LOGIN_CHANGED);
        }
    }

    private void changeData(@Nullable DataType dataType) {
        System.out.println(Constant.ENTER + dataType.toString());
        @NotNull String newData = "";
        while (newData.isEmpty()) {
            newData = Bootstrap.scanner.nextLine();
        }
        serviceLocator.getUserService().merge(serviceLocator.getUserService().getCurrentUser().getId(), newData, serviceLocator.getUserService().getCurrentUser(), dataType);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
