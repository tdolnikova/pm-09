package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;


public final class UserRegisterCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.USER_REG;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.USER_REG_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[REGISTRATION]");
        System.out.println(Constant.REG_ENTER_LOGIN);
        @NotNull String login = "";
        while (login.isEmpty()) {
            login = Bootstrap.scanner.nextLine();
        }
        System.out.println(Constant.REG_ENTER_PASSWORD);
        @NotNull String password = "";
        while (password.isEmpty()) {
            password = Bootstrap.scanner.nextLine();
        }
        if (serviceLocator.getUserService().findOneByName(login) != null) {
            System.out.println(Constant.REG_USER_EXISTS);
            return;
        }
        @NotNull final User newUser = new User(login, password, Role.USER);
        serviceLocator.getUserService().persist(newUser.getId(), newUser);
        System.out.println(Constant.REG_USER_REGISTERED);
    }

    @Override
    public boolean isSecure() {
        return serviceLocator.getUserService().getCurrentUser() == null;
    }
}
