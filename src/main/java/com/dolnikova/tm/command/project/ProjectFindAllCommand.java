package com.dolnikova.tm.command.project;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ProjectFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.FIND_ALL_PROJECTS;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.FIND_ALL_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() {
        if (!isSecure()) return;
        System.out.println("[PROJECT LIST]");
        @Nullable final List<Project> allProjects = serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId());
        if (allProjects.isEmpty()) System.out.println(Constant.NO_PROJECTS);
        else {
            for (final Project project : allProjects) {
                System.out.println(project.getId());
            }
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
