package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ProjectFindByDescriptionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.FIND_PROJECT_BY_DESCRIPTION;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.FIND_PROJECT_BY_DESCRIPTION_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @NotNull String userInput = "";
        System.out.println(Constant.INSERT_TEXT);
        while (userInput.isEmpty()) {
            userInput = Bootstrap.scanner.nextLine();
        }
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAllByDescription(serviceLocator.getUserService().getCurrentUser().getId(), userInput);
        for (final Project project : projectList) {
            System.out.println(project.getName());
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
