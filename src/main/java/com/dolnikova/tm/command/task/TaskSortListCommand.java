package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.SortingType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;

public final class TaskSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.SORT_TASKS;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.SORT_TASKS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser().getId());
        if (taskList == null || taskList.isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return;
        }
        System.out.println(Constant.CHOOSE_SORTING_TYPE + ":");
        for(final SortingType type : SortingType.values()) {
            System.out.println(type.displayName());
        }
        boolean typeChosen = false;
        while (!typeChosen) {
            @NotNull final String userChoice = Bootstrap.scanner.nextLine();
            if (userChoice.isEmpty()) return;
            if (userChoice.equals(SortingType.BY_STATUS.displayName())){
                typeChosen = true;
                Collections.sort(taskList, serviceLocator.getTaskService().getStatusComparator(true));
            }
            else if (userChoice.equals(SortingType.BY_CREATION_DATE.displayName())){
                typeChosen = true;
                Collections.sort(taskList, serviceLocator.getTaskService().getCreationDateComparator(true));
            }
            else if (userChoice.equals(SortingType.BY_START_DATE.displayName())){
                typeChosen = true;
                Collections.sort(taskList, serviceLocator.getTaskService().getStartDateComparator(true));
            }
            else if (userChoice.equals(SortingType.BY_END_DATE.displayName())){
                typeChosen = true;
                Collections.sort(taskList, serviceLocator.getTaskService().getEndDateComparator(true));
            }
            else {
                System.out.println(Constant.TRY_AGAIN);
            }
        }
        for (final Task task : taskList) {
            System.out.println(task.getId());
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}