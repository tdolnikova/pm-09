package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.REMOVE_TASK;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.REMOVE_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final Project project = findProject();
        if (project == null) return;
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser().getId());
        if (tasks == null || tasks.isEmpty()) {
            System.out.println(Constant.NO_TASKS);
            return;
        }
        @Nullable final List<Task> projectTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getProjectId().equals(project.getId())
                    && task.getOwnerId().equals(serviceLocator.getUserService().getCurrentUser().getId()))
                projectTasks.add(task);
        }
        if (projectTasks.isEmpty()) {
            System.out.println(Constant.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(Constant.IN_PROJECT + project.getName() + " " + projectTasks.size() + " " + Constant.OF_TASKS_WITH_POINT + ":");
        for (final Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Constant.INSERT_TASK_ID);
        boolean taskDeleted = false;
        while (!taskDeleted) {
            @NotNull final String taskIdToDelete = Bootstrap.scanner.nextLine();
            if (taskIdToDelete.isEmpty()) break;
            @Nullable final Task taskToRemove = serviceLocator.getTaskService().findOneById(serviceLocator.getUserService().getCurrentUser().getId(), taskIdToDelete);
            serviceLocator.getTaskService().remove(serviceLocator.getUserService().getCurrentUser().getId(), taskToRemove);
            System.out.println(Constant.TASK_DELETED);
            taskDeleted = true;
        }
    }

    private Project findProject() {
        if (serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_TASKS);
            return null;
        }
        if (serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return null;
        }
        System.out.println(Constant.CHOOSE_PROJECT);
        @Nullable Project project = null;
        while (project == null) {
            @NotNull final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectService().findOneById(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }

}
