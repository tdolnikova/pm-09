package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class TaskFindByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.FIND_TASK_BY_NAME;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.FIND_TASK_BY_NAME_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @NotNull String userInput = "";
        System.out.println(Constant.INSERT_TEXT);
        while (userInput.isEmpty()) {
            userInput = Bootstrap.scanner.nextLine();
        }
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAllByName(serviceLocator.getUserService().getCurrentUser().getId(), userInput);
        for (final Task task : taskList) {
            System.out.println(task.getName());
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}