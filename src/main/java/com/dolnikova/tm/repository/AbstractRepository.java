package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    final Map<String, E> entities = new LinkedHashMap<>();

    AbstractRepository() {
    }

    @Nullable
    @Override
    public E findOneById(@NotNull String ownerId, @NotNull String id) {
        for (Map.Entry<String, E> entity : entities.entrySet()) {
            @NotNull final E foundEntity = entity.getValue();
            if (id.equals(foundEntity.getId())
                    && entity.getValue().getOwnerId().equals(ownerId))
                return foundEntity;
        }
        return null;
    }

    @Override
    public E findOneByName(@NotNull String name) {
        for (Map.Entry<String, E> entity : entities.entrySet()) {
            @NotNull final E foundEntity = entity.getValue();
            if (name.equals(foundEntity.getName()))
                return foundEntity;
        }
        return null;
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String ownerId) {
        @Nullable final List<E> all = new ArrayList<>();
        for (Map.Entry<String, E> entity : entities.entrySet()) {
            if (entity.getValue().getOwnerId().equals(ownerId)) all.add(entity.getValue());
        }
        return all;
    }

    @Override
    public @Nullable List<E> findAllByName(@NotNull String ownerId, @NotNull String text) {
        List<E> entityList = new ArrayList<>();
        for (Map.Entry<String, E> entity : entities.entrySet()) {
            @NotNull final E foundEntity = entity.getValue();
            if (foundEntity.getName().contains(text)
                    && entity.getValue().getOwnerId().equals(ownerId))
                entityList.add(foundEntity);
        }
        return entityList;
    }

    @Override
    public @Nullable List<E> findAllByDescription(@NotNull String ownerId, @NotNull String text) {
        List<E> entityList = new ArrayList<>();
        for (Map.Entry<String, E> entity : entities.entrySet()) {
            @NotNull final E foundEntity = entity.getValue();
            if (foundEntity.getDescription().contains(text)
                    && entity.getValue().getOwnerId().equals(ownerId))
                entityList.add(foundEntity);
        }
        return entityList;
    }

    @Override
    public void persist(@NotNull final String ownerId, @NotNull final E entity) {
        if (entity.getOwnerId().equals(ownerId)) {
            entities.put(entity.getId(), entity);
        }
    }

    @Override
    public void merge(@NotNull String ownerId, @NotNull String newData, @NotNull E entityToMerge, @NotNull DataType dataType) {
        if (!entityToMerge.getOwnerId().equals(ownerId)) return;
        switch (dataType) {
            case NAME:
                entityToMerge.setName(newData);
                break;
            case DESCRIPTION:
                if (entityToMerge.getClass().isAssignableFrom(User.class)) {
                    entityToMerge.setDescription(HashUtil.stringToHashString(newData));
                } else {
                    entityToMerge.setDescription(newData);
                }
                entities.put(entityToMerge.getOwnerId(), entityToMerge);
                break;
        }
        entities.put(entityToMerge.getOwnerId(), entityToMerge);
    }

    @Override
    public void remove(@NotNull final String ownerId, @NotNull final E entity) {
        if (entity.getOwnerId().equals(ownerId))
            entities.remove(entity.getId());
    }

    @Override
    public void removeAll(@NotNull final String ownerId) {
        for (Iterator<Map.Entry<String, E>> it = entities.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, E> entry = it.next();
            if (entry.getValue().getOwnerId().equals(ownerId)) {
                it.remove();
            }
        }
    }
}
