package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.entity.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository() {
    }

}
