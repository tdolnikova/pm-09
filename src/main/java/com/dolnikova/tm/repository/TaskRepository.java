package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.entity.Task;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository() {
    }

}
