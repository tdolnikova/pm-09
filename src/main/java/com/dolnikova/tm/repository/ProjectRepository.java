package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.entity.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
    }

}
