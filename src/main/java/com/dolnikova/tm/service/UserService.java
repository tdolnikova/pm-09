package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    private IUserRepository userRepository;
    private User currentUser = null;

    public UserService() {
    }

    public UserService(@NotNull final IUserRepository taskRepository) {
        super(taskRepository);
        this.userRepository = taskRepository;
    }

    @Nullable
    @Override
    public User findOneById(@Nullable String ownerId, @Nullable String id) {
        if (ownerId == null || ownerId.isEmpty() || id == null || id.isEmpty()) return null;
        return userRepository.findOneById(ownerId, id);
    }

    @Override
    public User findOneByName(@Nullable String name) {
        if (name == null || name.isEmpty()) return null;
        return userRepository.findOneByName(name);
    }

    @Override
    public @Nullable List<User> findAll(@Nullable String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return userRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<User> findAllByName(@Nullable String ownerId, @Nullable String text) {
        if (ownerId == null || ownerId.isEmpty() || text == null || text.isEmpty()) return null;
        return userRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<User> findAllByDescription(@Nullable String ownerId, @Nullable String text) {
        if (ownerId == null || ownerId.isEmpty() || text == null || text.isEmpty()) return null;
        return userRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable String ownerId, @Nullable User entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        userRepository.persist(ownerId, entity);
    }

    @Override
    public void merge(@Nullable String ownerId, @Nullable String newData, @Nullable User entityToMerge, @Nullable DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()
                || newData == null || newData.isEmpty()
                || entityToMerge == null || dataType == null) return;
        userRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable String ownerId, @Nullable User entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        userRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        userRepository.removeAll(ownerId);
    }

    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(@Nullable final User currentUser) {
        this.currentUser = currentUser;
    }
}
