package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.api.service.IService;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> abstractRepository;

    AbstractService() {
    }

    AbstractService(@NotNull final IRepository<E> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    @Override
    public E findOneById(@Nullable String ownerId, @Nullable String id) {
        if (ownerId == null || ownerId.isEmpty() || id == null || id.isEmpty()) return null;
        return abstractRepository.findOneById(ownerId, id);
    }

    @Override
    public E findOneByName(@Nullable String name) {
        if (name == null || name.isEmpty()) return null;
        return abstractRepository.findOneByName(name);
    }

    @Override
    public @Nullable List<E> findAll(@Nullable String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return abstractRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<E> findAllByName(@Nullable String ownerId, @Nullable String text) {
        if (ownerId == null || ownerId.isEmpty() || text == null || text.isEmpty()) return null;
        return abstractRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<E> findAllByDescription(@Nullable String ownerId, @Nullable String text) {
        if (ownerId == null || ownerId.isEmpty() || text == null || text.isEmpty()) return null;
        return abstractRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable String ownerId, @Nullable E entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        abstractRepository.persist(ownerId, entity);
    }

    @Override
    public void merge(@Nullable String ownerId, @Nullable String newData, @Nullable E entityToMerge, @Nullable DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()
                || newData == null || newData.isEmpty()
                || entityToMerge == null || dataType == null) return;
        abstractRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable String ownerId, @Nullable E entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        abstractRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        abstractRepository.removeAll(ownerId);
    }
}
