package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService() {
    }

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String ownerId, @Nullable String id) {
        if (ownerId == null || ownerId.isEmpty() || id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(ownerId, id);
    }

    @Override
    public Task findOneByName(@Nullable String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findOneByName(name);
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return taskRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<Task> findAllByName(@Nullable String ownerId, @Nullable String text) {
        if (ownerId == null || ownerId.isEmpty() || text == null || text.isEmpty()) return null;
        return taskRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<Task> findAllByDescription(@Nullable String ownerId, @Nullable String text) {
        if (ownerId == null || ownerId.isEmpty() || text == null || text.isEmpty()) return null;
        return taskRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable String ownerId, @Nullable Task entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        taskRepository.persist(ownerId, entity);
    }

    @Override
    public void merge(@Nullable String ownerId, @Nullable String newData, @Nullable Task entityToMerge, @Nullable DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()
                || newData == null || newData.isEmpty()
                || entityToMerge == null || dataType == null) return;
        taskRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable String ownerId, @Nullable Task entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        taskRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        taskRepository.removeAll(ownerId);
    }

    @NotNull
    @Override
    public Comparator<Task> getStatusComparator(boolean direction) {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getStatus().compareTo(o2.getStatus());
            }
        };
    }

    @NotNull
    @Override
    public Comparator<Task> getCreationDateComparator(boolean direction) {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getCreationDate().compareTo(o2.getCreationDate());
            }
        };
    }

    @NotNull
    @Override
    public Comparator<Task> getStartDateComparator(boolean direction) {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        };
    }

    @NotNull
    @Override
    public Comparator<Task> getEndDateComparator(boolean direction) {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getEndDate().compareTo(o2.getEndDate());
            }
        };
    }

}
