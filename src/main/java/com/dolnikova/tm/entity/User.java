package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.HashUtil;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public final class User extends AbstractEntity {

    @Nullable
    private String id;
    @Nullable
    private String ownerId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Role role;

    public User(@Nullable final String name, @Nullable final String description, @Nullable final Role role) {
        this.id = UUID.randomUUID().toString();
        ownerId = this.id;
        this.name = name;
        this.description = HashUtil.stringToHashString(description);
        this.role = role;
    }

}
