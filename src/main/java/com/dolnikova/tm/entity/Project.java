package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Status;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public final class Project extends AbstractEntity {

    @Nullable
    private String id;
    @Nullable
    private String ownerId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date creationDate;
    @Nullable
    private Date startDate;
    @Nullable
    private Date endDate;
    @Nullable
    private Status status;

    public Project() {
    }

    public Project(@Nullable final String ownerId, @Nullable final String projectName) {
        this.name = projectName;
        this.ownerId = ownerId;
        id = UUID.randomUUID().toString();
        status = Status.PLANNED;
        creationDate = new Date();
        description = "some text you need to find";
    }

}
